//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	    _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2017 - All Rights Reserved
//


"use strict"

function ChatForm(chatbot){

	var input, form, submit, chat;

	function init(){
		form = document.getElementById('chatForm');
		input = document.getElementById('msg');
		submit = document.getElementById('chatSubmit');
		chat = document.getElementById('chat');
		submit.addEventListener("click", processForm);
	}

	var processForm = function(e){
		console.log("processForm");
		e.preventDefault();

		//add message
		chat.innerHTML += "<p class='me'>" + input.value + "</p>";

		//send message
		chatbot.sendText(input.value);
		input.value = "";
	}

	init();
}