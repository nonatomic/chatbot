//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	    _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2017 - All Rights Reserved
//


"use strict"

function Chatbot(token){

	var enableTextToSpeech = true;
	var chat, streamClient, client, input;

	function init(){
		chat = document.getElementById('chat');
		input = document.getElementById('msg');
		client = new ApiAi.ApiAiClient({accessToken: token, streamClientClass: ApiAi.ApiAiStreamClient});

		streamClient = client.createStreamClient();
		streamClient.init();

		streamClient.onInit = function() {
			console.log("> ON INIT use direct assignment property");
			streamClient.open();
		};

		streamClient.onStartListening = function() {
			console.log("> ON START LISTENING");
		};

		streamClient.onStopListening = function() {
			console.log("> ON STOP LISTENING");
		};

		streamClient.onOpen = function() {
			console.log("> ON OPEN SESSION");
		};

		streamClient.onClose = function() {
			console.log("> ON CLOSE");
			streamClient.close();
		};

		streamClient.onResults = function streamClientOnResults(results) {
			console.log("> ON RESULTS", results);
		}

		streamClient.onError = function(code, data) {
			streamClient.close();
			console.log("> ON ERROR", code, data);
		};
		
		streamClient.onEvent = function(code, data) {
			console.log("> ON EVENT", code, data);
		};
	}	

	//public methods
	this.sendText = function(text) {
		var promise = client.textRequest(text);
		var t = this;
    	promise.then(function(serverResponse){
			console.log("THEN: ", serverResponse);
			var res = serverResponse.result;
			var action = res.action;
			var response = res.fulfillment.speech;
			var params = res.parameters.Navigation;
			var stat = serverResponse.stastus;

			chat.innerHTML += "<p class='them'>" + response + "</p>";

			if(enableTextToSpeech){
				console.log(t.tts(response));
			}
		})
    	promise.catch(function(serverError){
			console.log("CATCH:", serverError);
		});
	}

	this.tts = function(text) {
		return client.ttsRequest(text);
	}

	this.startMic = function() {
		streamClient.startListening();
	}

	this.stopMic = function() {
		streamClient.stopListening();
	}

	init();
}	
