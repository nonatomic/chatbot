# README #

Basic bot with the api.ai's javascript api

# How do I get set up? ###

* Visit https://api.ai/ and create yourself a bot
* Add your Client access token into the index.html
* Deploy to a server a BINGO!

# Nonatomic

* [Produced by @paulstamp](https://twitter.com/paulstamp)
* [Nonatomic Website](https://www.nonatomic.co.uk)
